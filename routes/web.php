<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use Symfony\Component\HttpFoundation\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/storage/{extra}', function ($extra) {
//     return redirect("/public/st  orage/$extra");
//     })->where('extra', '.*');

Route::get('/', [PagesController::class, 'index'])->name('home');
Route::get('/product/{id}', [PagesController::class, 'product'])->name('product');
Route::get('/cart', [PagesController::class, 'cart'])->name('cart');
Route::get('/checkout', [PagesController::class, 'checkout'])->middleware(['auth'])->name('checkout');
Route::get('/shop', [PagesController::class, 'shop'])->name('shop');

Route::post('/pay', [PagesController::class, 'initialize'])->middleware(['auth'])->name('pay');
Route::get('/pay', function (){
    return redirect()->route("pay");
})->middleware(['auth'])->name('getPay');
Route::any('/rave/callback', [PagesController::class, 'callback'])->name('callback');
Route::get('/success', [PagesController::class, 'success'])->middleware(['auth'])->name('success');
// Route::post('/receivepayment', [PagesController::class, 'webhook'])->name('webhook');
// Route::any('/rave/callback', [PagesController::class, 'callback'])->name('callback'

Route::get('/paystack/verify/{reference}',[PagesController::class,"completeCheckout"]);

Route::get("testRedirect", function(Request $request){
    return $request;
});



Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
