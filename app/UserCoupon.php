<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserCoupon extends Model
{
    public function coupon()
    {
        return $this->belongsTo(Coupon::class,'coupon_id');

    }
}
