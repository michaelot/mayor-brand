<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Gloudemans\Shoppingcart\Facades\Cart;

class Product extends Component
{

    public $product;
    // public $duplicateWish;
    public $liked= false;

    public function mount(){
        $product = $this->product;
        $duplicateWish = Cart::search(function ($cartItem, $rowId) use ($product) {
            return $cartItem->id === $product->id;
        });
        if($duplicateWish->isNotEmpty()){
            $this->liked = true;
        }else{
            $this->liked = false;
        }
    }

    public function addToWishList(){
        $product = $this->product;
        $duplicateWish = Cart::instance('wishlist')->search(function ($cartItem, $rowId) use ($product) {
            return $cartItem->id === $product->id;
        });
        // dd($duplicateWish);
        if($duplicateWish->isNotEmpty()){
            Cart::instance('wishlist')->remove($duplicateWish->take(1)->keys()->first());
            $this->liked = false;
        }else{
            $this->liked = true;
            Cart::instance('wishlist')->add($this->product->id, $this->product->name, 1, $this->product->price)
            ->associate('App\Product');
        }
            $this->emitTo('header','increaseCart');

    }

    public function render()
    {
        return view('livewire.product');
    }
}
