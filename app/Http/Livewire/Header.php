<?php

namespace App\Http\Livewire;

use App\Order;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;


class Header extends Component
{
    public $activeLink = "";

    public $cart;
    public $wishlist;
    public $userOrders;
    public $cartItems;
    public $wishlistItems;
    public $cartTotal;
    public $showCart= true;



    protected $listeners = ['increaseCart' => 'cartCount'];

    public $version = "";

    public function switchCart($which){
        if($which == "cart"){
            $this->showCart = true;
            $this->cartItems = Cart::instance('default')->content();
            // $this->cartTotal = Cart::instance('default')->total();
            // $this->cart = Cart::instance('default')->count();
        }else if($which == "wishlist"){
            $this->showCart = false;
            // $this->wishlist = Cart::instance('wishlist')->count();
            $this->wishlistItems = Cart::instance('wishlist')->content();
        }
        // $this->doDispatch();
    }

    public function mount(){
        $this->cartItems = Cart::instance('default')->content();
        $this->cartTotal = Cart::instance('default')->total();
        $this->cart = Cart::instance('default')->count();
        $this->wishlist = Cart::instance('wishlist')->count();
        $this->wishlistItems = Cart::instance('wishlist')->content();

    }

    public function cartCount(){
        $this->cart = Cart::instance('default')->count();
        $this->cartItems = Cart::instance('default')->content();
        $this->cartTotal = Cart::instance('default')->total();
        $this->wishlist = Cart::instance('wishlist')->count();
        $this->doDispatch();
    }

    public function render()
    {
        return view('livewire.header');
    }
    public function doDispatch(){
        $this->dispatchBrowserEvent('contentChanged');
    }
}
