<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\UserCoupon;
use App\Coupon;

class CartView extends Component
{

    public $count;
    public $test=0;
    public $content;
    public $wishCount;
    public $wishContent;
    public $coupon;
    public $successMsg="";
    public $errorMsg="";


    protected $listeners = ['doDelete' => 'updateCount','doCartRefresh' => 'doRefresh'];

    public function mount(){
        $this->count =Cart::instance('default')->count();
        $this->content =Cart::instance('default')->content();

    }

    public function doRefresh(){
        $this->count =Cart::instance('default')->count();
        $this->content =Cart::instance('default')->content();

    }

    public function updateCount($data){
        // $this->test = $data['rowId'];
        Cart::instance('default')->remove($data['rowId']);
        $this->emitTo('header','increaseCart');
        $this->count =Cart::instance('default')->count();
        $this->content =Cart::instance('default')->content();
    }

    public function clearCart(){
        $this->test = $this->test+1;
        Cart::instance('default')->destroy();
        $this->emitTo('header','increaseCart');
        $this->count =Cart::instance('default')->count();
        $this->content =Cart::instance('default')->content();
    }

    public function applyCoupon()
    {
        
        if( Coupon::where("coupon",$this->coupon)->exists()){
            $coupon = Coupon::where("coupon",$this->coupon)->first();
            $usedCoupon = UserCoupon::where("user_id", auth()->user()->id)->where("coupon_id", $coupon->id)->exists();
            if($usedCoupon){
                $this->errorMsg = "This coupon has been used by you";
                $this->successMsg = "";
            }else{

                if( Cart::instance('default')->count() >= $coupon->min_cart_items){

                    $userCoupon = new UserCoupon();
                    $userCoupon->user_id = auth()->user()->id;
                    $userCoupon->coupon_id = $coupon->id;
                    $userCoupon->status = 0;
                    $userCoupon->save();
                    
                    $this->errorMsg = "";
                    $this->successMsg = "Coupon Added to cart. Updated Price will reflect on checkout";

                }else{
                    $this->errorMsg = "You must have at least $coupon->min_cart_items items in your cart to use this coupon";
                    $this->successMsg = "";
                }

            }
        }else{
            $this->errorMsg = "Coupon Does not Exist";
            $this->successMsg = "";
        }

        $this->coupon ="";
        $this->count =Cart::instance('default')->count();
        $this->content =Cart::instance('default')->content();
    }

    public function render()
    {
        return view('livewire.cart-view');
    }
}
