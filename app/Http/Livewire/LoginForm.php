<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class LoginForm extends Component
{
    public $email;
    public $password;

    protected $rules = [
        'email' => 'required|email',
        'password' => 'required',
    ];

    public function login()
    {
        $validatedData = $this->validate();

        if (Auth::attempt(array('email' => $this->email, 'password' => $this->password))) {
            return redirect('/');
        } else {
            $this->addError('email', 'Incorrect email and password.');
        }
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function render()
    {

        return view('livewire.login-form');
    }
}
