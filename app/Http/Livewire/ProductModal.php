<?php

namespace App\Http\Livewire;

use App\Order;
use Livewire\Component;
use Illuminate\Http\Request;
use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Session;


class ProductModal extends Component
{
    public $product;
    public $color ="";
    public $size="";
    public $quantity=1;
    public $gender="Male";
    public $successMsg="";
    public $showWishCount = false;
    public $wishList;
    public $showOrders = false;
    public $isInCart= false;
    protected $listeners = ['showModal' => 'showProduct','showWishModal' => 'showWishList','showOrderModal' => 'showOrderList'];


    public function mount(){
        // $this->orders
    }

    public function showProduct(Product $product){
        $this->successMsg ="";
        $this->product = $product;
        $this->isInCart = false;
        $this->doDispatch();
        // $this->emitTo('product-details','RefreshComp',$this->product);
    }
    public function showWishList(){
        // $this->product = $product;
        $this->showWishCount =true;
        $this->product =null;
        $this->doDispatch();
        // $this->emitTo('product-details','RefreshComp',$this->product);
    }

    public function showOrderList(){
        // $this->product = $product;
        $this->showWishCount =false;
        $this->product =null;
        $this->showOrders = true;
        $this->doDispatch();
        // $this->emitTo('product-details','RefreshComp',$this->product);
    }

    public function setColorVal($val){
        $this->color = $val;
        $this->doDispatch();
    }

    public function setSizeVal($val){
        $this->size = $val;
        $this->doDispatch();
    }

    public function setGenderVal($val){
        $this->gender = $val;
        $this->doDispatch();
    }

    public function increaseQuantity(){
        $this->quantity++;
        $this->doDispatch();
    }

    public function reduceQuantity(){
        if($this->quantity > 0){
            $this->quantity--;
        }
        $this->doDispatch();
    }

    public function addToCart(){
        $product = $this->product;
        // dd($this->color."----".$this->size.'----'.$this->quantity.'----'.$this->product->price);
        $this->doDispatch();
        $data = $this->validate([
            'color' => 'required',
            'size' => 'required',
            'quantity' => 'required',
            'gender' => 'required'
        ]);
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($product) {
            return $cartItem->id === $product->id;
        });
        // if($duplicates->isNotEmpty()){
        //     $this->successMsg =" Item a already in your cart";
        // }else{
            Cart::instance('default')->add($this->product->id, $this->product->name, (int)$this->quantity, $this->product->price,['size'=> $this->size,'color'=>$this->color,'gender' => $this->gender])
            ->associate('App\Product');
            $this->successMsg =" Added to cart Successfully";
        // }


        $this->emitTo('header','increaseCart');
        $this->doDispatch();
        $this->isInCart = true;
        // return Cart::content();
    }

    public function closeModal(){
        Session::forget('error');
        $this->showWishCount = false;
        $this->showOrders = false;
        $this->successMsg ="";
        $this->product =null;
        $this->size="";
        $this->color="";
        $this->quantity=1;


    }

    public function doDispatch(){
        $this->dispatchBrowserEvent('contentChanged');
    }

    public function render()
    {
        return view('livewire.product-modal');
    }
}
