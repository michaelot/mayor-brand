<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Product;

class ProductDetails extends Component
{

    public $product;

    public $randomProduct;

    protected $listeners = ['RefreshComp' => 'showProduct'];

    public function showProduct(Product $product){
        $this->product = $product;
        // $this->emitTo('productDetails','RefreshComp',$this->product);
    }

    public function render()
    {
        return view('livewire.product-details');
    }
}
