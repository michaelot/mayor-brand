<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Welcome extends Component
{

    public $appName = "";

    public function render()
    {
        return view('livewire.welcome');
    }
}
