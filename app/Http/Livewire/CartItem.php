<?php

namespace App\Http\Livewire;

use Gloudemans\Shoppingcart\Facades\Cart;
use Livewire\Component;

class CartItem extends Component
{
    public $rowId;
    public $row;
    public $quantity;

    public function mount(){
        $this->quantity = Cart::get($this->rowId)->qty;
    }

    public function deleteProduct(){
        $this->emitTo('cart-view','doDelete',['rowId'=>$this->rowId]);
    }

    public function increaseQuantity(){
        $this->quantity++;
        Cart::instance('default')->update($this->rowId, $this->quantity);
        $this->emitTo('cart-view','doCartRefresh');
    }

    public function decreaseQuantity(){
        $this->quantity--;
        Cart::instance('default')->update($this->rowId, $this->quantity);
        $this->emitTo('cart-view','doCartRefresh');
    }

    public function render()
    {
        return view('livewire.cart-item');
    }
}
