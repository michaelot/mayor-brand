<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\ProductCategory;
use App\Newsletter;

class Footer extends Component
{

    public $categories = [];
    public $email;
    public $succes;
    

    public function mount(){
        $this->categories = ProductCategory::all();
    }

    public function saveNewsletter()
    {
        if(!Newsletter::where("email",$this->email)->exists()){
            $theMail = new Newsletter();
            $theMail->email = $this->email;
            $theMail->save();
        }

        session()->flash('newMessage', 'Thank you for subscribing.');

    }
    
    public function render()
    {
        return view('livewire.footer');
    }
}
