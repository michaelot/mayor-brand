<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Product;
use App\ProductCategory;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use KingFlamez\Rave\Facades\Rave;
use App\UserCoupon;
use App\Models\User;
use App\Coupon;



class PagesController extends Controller
{
    public function index(){
        $newproducts = Product::take(8)->orderBy('created_at')->get();

        $categories = ProductCategory::take(3)->get();

        // return $categories;

        return view('pages.index',['newproducts' => $newproducts, 'categories' => $categories ]);
    }

    public function product($id){
        // Cart::add('293ad', 'Product '.$id, 1, 9.99);
         $duplicateWish = Cart::instance('wishlist')->search(function ($cartItem, $rowId) {
            return $cartItem->id === 1;
        });
        if($duplicateWish->isNotEmpty()){
            return $duplicateWish->take(1)->keys()->first();
        }else{
            return "empty";
        }
        return Cart::instance('wishlist')->content();

        // $product = Product::find($id);
        // $randomProduct = Product::where('id','!=',$id)->inRandomOrder()->take(4)->get();
        // return view('pages.product',['product' => $product,'randomProduct' => $randomProduct]);
    }

    public function shop(){
        $categories = ProductCategory::all();
        $products = Product::inRandomOrder()->get();
        return view('pages.shop',[
            'products' => $products,
            'categories' => $categories
        ]);
    }

    public function cart(){
        return view('pages.cart');
    }
    public function checkout(Request $request)
    {
        $discount = 0;
        if(UserCoupon::where("user_id", auth()->user()->id)->where("status",0)->exists()){

            $userCoupon = UserCoupon::where("user_id", auth()->user()->id)->where("status",0)->first();

            if(Coupon::where("id",$userCoupon->coupon_id)->exists()){

                $coupon = Coupon::where("id",$userCoupon->coupon_id)->first();

                if( Cart::instance('default')->count() >= $coupon->min_cart_items){
                    $discount = (int)$userCoupon->coupon->discount;
                    session(['coupon' => $userCoupon->id]);
                }else{
                    $discount = 0;
                    $request->session()->put('coupon', null);
                }

            }else{

                $userCoupon->status = 1;
                $userCoupon->save();

            }

        }else{
            $discount = 0;
        }
        return view('pages.checkout')->with(["coupon" => $discount]);
    }

    public function initialize(Request $request)
    {

        $user = User::find(auth()->user()->id);
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->save();

        if($request->state != "Lagos" )
        {
            $request->amount = $request->amount+3000;
        }

        // $request->amount = 3000;

        if(UserCoupon::where("user_id", auth()->user()->id)->where("status",0)->exists()){
            $userCoupon = UserCoupon::where("user_id", auth()->user()->id)->where("status",0)->first();
            if(Coupon::where("id",$userCoupon->coupon_id)->exists()){
                $coupon = Coupon::where("id",$userCoupon->coupon_id)->first();

                if( Cart::instance('default')->count() >= $coupon->min_cart_items){
                    session(['coupon' => $userCoupon->id]);
                }else{
                    $request->session()->put('coupon', null);
                }
            }else{
                $userCoupon->status = 1;
                $userCoupon->save();
            }


        }

        //This initializes payment and redirects to the payment gateway
        //The initialize method takes the parameter of the redirect URL
        Rave::initialize(route('callback'));
    }

    public function callback(Request $request)
    {

        $response = json_decode($request['resp']);
        // dd();
        $data = Rave::verifyTransaction($response->tx->txRef);

        // dd($data->data->chargecode);

        $chargeResponsecode = $data->data->chargecode;
        $chargeAmount = $data->data->amount;
        $chargeCurrency = $data->data->currency;

        // $amount = Cart::instance('default')->total();
        $currency = "NGN";


        if (($chargeResponsecode == "00" || $chargeResponsecode == "0")  && ($chargeCurrency == $currency)) {
            // transaction was successful...
            // please check other things like whether you already gave value for this ref
            // if the email matches the customer who owns the product etc
            //Give Value and return to Success page
            // return "Successful Payment";

            // return dd();
            $products =  Cart::instance('default')->content()->map(function ($item){
                return $item->model->name.', Quantity: '.$item->qty.', Size:'. $item->options->size.', Color:'. $item->options->color.', Gender:'. $item->options->gender;
            })->values()->toJson();
            $order = new Order();
            $order->title = uniqid("order-");
            $order->amount = $data->data->amount;
            $order->cart = json_encode(Cart::instance('default')->content());
            $order->qty = Cart::instance('default')->count();
            $order->products = $products;
            $order->user = Auth::user()->id;
            if ($request->session()->has('coupon')) {
                $userCoupon = UserCoupon::where("id",$request->session()->get('coupon'))->first();
                $userCoupon->status = 1;
                $userCoupon->save();
                $request->session()->put('coupon', null);
                $order->coupon = $userCoupon->coupon_id;
            }
            $order->save();
            Cart::instance('default')->destroy();
            // return "Successful Payment";

            return redirect()->route('success')->with(['error' => false]);

        } else {
            //Dont Give Value and return to Failure page
            return redirect('/success')->with(['error' => true]);
            return "unSuccessful Payment";

            // return redirect('/failed');
        }


    }

    public function completeCheckout( $reference, Request $request){
        // return json_decode($request->details)->state;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/verify/".$reference,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer sk_live_68332ec2fb5faf8db0287799c715cf146a6e1c30",
            "Cache-Control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err ." Refresh page";
        } else {
            // return json_decode($response)->data->amount/100;
            $products =  Cart::instance('default')->content()->map(function ($item){
                return $item->model->name.', Quantity: '.$item->qty.', Size:'. $item->options->size.', Color:'. $item->options->color.', Gender:'. $item->options->gender;
            })->values()->toJson();
            $order = new Order();
            $order->title = uniqid("order-");
            $order->amount = json_decode($response)->data->amount/100;
            $order->cart = json_encode(Cart::instance('default')->content());
            $order->qty = Cart::instance('default')->count();
            $order->products = $products;
            $order->user = Auth::user()->id;
            if ($request->session()->has('coupon')) {
                $userCoupon = UserCoupon::where("id",$request->session()->get('coupon'))->first();
                $userCoupon->status = 1;
                $userCoupon->save();
                $request->session()->put('coupon', null);
                $order->coupon = $userCoupon->coupon_id;
            }
            $order->save();
            $user = User::find(auth()->user()->id);
            $user->address = json_decode($request->details)->address;
            $user->phone = json_decode($request->details)->phone;
            $user->city = json_decode($request->details)->city;
            $user->state = json_decode($request->details)->state;
            $user->save();
            Cart::instance('default')->destroy();
            // return "Successful Payment";

            return redirect()->route('success')->with(['error' => false]);
        }


    }

    public function success(){

        return view('pages.success');
    }


}
