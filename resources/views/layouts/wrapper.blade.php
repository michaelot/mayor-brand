<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{ asset('images/theFav.png') }}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/bootstrap/css/bootstrap.min.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="  {{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('fonts/iconic/css/material-design-iconic-font.min.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('fonts/linearicons-v1.0.0/icon-font.min.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/animate/animate.css') }} ">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/css-hamburgers/hamburgers.min.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/animsition/css/animsition.min.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/select2/select2.min.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/daterangepicker/daterangepicker.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/slick/slick.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/MagnificPopup/magnific-popup.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('designVendor/perfect-scrollbar/perfect-scrollbar.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" {{ asset('css/util.css') }} " data-turbolinks-track="true">
	<link rel="stylesheet" type="text/css" href=" {{ asset('css/main.css') }} " data-turbolinks-track="true">
<!--===============================================================================================-->

@livewireStyles
@livewireScripts

<script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js"></script>
</head>
<body class="animsition">

    @yield('content')

	<!-- Footer -->
	@livewire("footer")

    {{-- cart --}}
    <div class="wrap-header-cart js-panel-cart">
		<div class="s-full js-hide-cart"></div>
        @livewire('cart-modal')
	</div>


	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>

    <div class="wrap-modal1 js-modal1 p-t-60 p-b-20 ">
        {{-- @if() --}}
            @livewire('product-modal')
        {{-- @else

        @endif --}}
    </div>




<!--===============================================================================================-->
<script src=" {{ asset('designVendor/jquery/jquery-3.2.1.min.js') }} " data-turbolinks-track="true"></script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/animsition/js/animsition.min.js') }} " data-turbolinks-track="true"></script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/bootstrap/js/popper.js') }} " data-turbolinks-track="true"></script>
	<script src=" {{ asset('designVendor/bootstrap/js/bootstrap.min.js') }} " data-turbolinks-track="true"></script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/select2/select2.min.js') }} " data-turbolinks-track="true"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/daterangepicker/moment.min.js') }} " data-turbolinks-track="true"></script>
	<script src=" {{ asset('designVendor/daterangepicker/daterangepicker.js') }} " data-turbolinks-track="true"></script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/slick/slick.min.js') }} " data-turbolinks-track="true"></script>
	<script src=" {{ asset('js/slick-custom.js') }} " data-turbolinks-track="true"></script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/parallax100/parallax100.js') }} " data-turbolinks-track="true"></script>
	<script>
        $('.parallax100').parallax100();
	</script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/MagnificPopup/jquery.magnific-popup.min.js') }} " data-turbolinks-track="true"></script>
	<script>
		$('.gallery-lb').each(function() { // the containers for all your galleries
			$(this).magnificPopup({
		        delegate: 'a', // the selector for gallery item
		        type: 'image',
		        gallery: {
		        	enabled:true
		        },
		        mainClass: 'mfp-fade'
		    });
		});
	</script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/isotope/isotope.pkgd.min.js') }} " data-turbolinks-track="true"></script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/sweetalert/sweetalert.min.js') }} " data-turbolinks-track="true"></script>
	<script data-turbolinks-track="true">
		$('.js-addwish-b2').on('click', function(e){
			e.preventDefault();
		});

		$('.js-addwish-b2').each(function(){
			var nameProduct = $(this).parent().parent().find('.js-name-b2').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");

				$(this).addClass('js-addedwish-b2');
				$(this).off('click');
			});
		});

		$('.js-addwish-detail').each(function(){
			var nameProduct = $(this).parent().parent().parent().find('.js-name-detail').html();

			$(this).on('click', function(){
				swal(nameProduct, "is added to wishlist !", "success");

				$(this).addClass('js-addedwish-detail');
				$(this).off('click');
			});
		});

		/*---------------------------------------------*/

		$('.js-addcart-detail').each(function(){
			var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
			$(this).on('click', function(){
				swal(nameProduct, "is added to cart !", "success");
			});
		});

	</script>
<!--===============================================================================================-->
	<script src=" {{ asset('designVendor/perfect-scrollbar/perfect-scrollbar.min.js') }}" data-turbolinks-track="true"></script>
	<script>
		$('.js-pscroll').each(function(){
			$(this).css('position','relative');
			$(this).css('overflow','hidden');
			var ps = new PerfectScrollbar(this, {
				wheelSpeed: 1,
				scrollingThreshold: 1000,
				wheelPropagation: false,
			});

			$(window).on('resize', function(){
				ps.update();
			})
		});
	</script>
<!--===============================================================================================-->
	<script src=" {{ asset('js/main.js') }}" data-turbolinks-track="true"></script>

	<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/611296c2d6e7610a49af89eb/1fco9pq34';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
	</script>
	<!--End of Tawk.to Script-->
</body>
</html>
