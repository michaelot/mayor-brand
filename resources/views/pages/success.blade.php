@extends('layouts.wrapper')
@section('content')
    @livewire('header', ['activeLink' => "cart","version"=>"header-v4"])
    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
                Home
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
                Checkout
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <span class="stext-109 cl4">
                Success
            </span>
        </div>
    </div>



	<!-- Shoping Cart -->
		<div class="container">
			<div class="row">

				<div class="col-sm-10 m-t-50 col-lg-10 col-xl-10 m-lr-auto m-b-50">
					<div class="text-center bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">

                        {{-- @if ($error)
                            <h4 class="text-center mtext-109 cl2 p-b-30 m-t-30">
                                Your Payment Wasn't Successful
                            </h4>
                        @else --}}
                            <img width="200" src={{ asset('images/checked.png') }}>

                            <h4 class="text-center mtext-109 cl2 p-b-30 m-t-30">
                                Your Payment Was Successful
                            </h4>
                        {{-- @endif --}}

						<a href=" {{route('home')}} " class="mt-5 flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
							Continue Shopping
                        </a>
					</div>
				</div>
			</div>
		</div>
@endsection
