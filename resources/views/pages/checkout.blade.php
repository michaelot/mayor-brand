@extends('layouts.wrapper')
@section('content')
    @livewire('header', ['activeLink' => "cart","version"=>"header-v4"])
    <!-- breadcrumb -->
    <div class="container">
        <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
            <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
                Home
                <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
            </a>

            <span class="stext-109 cl4">
                Checkout
            </span>
        </div>
    </div>



	<!-- Shoping Cart -->
    {{-- <form class="bg0 p-t-75 p-b-85" method="POST" action="{{ route('pay') }}"> --}}
    <form id="paystcdForm" class="bg0 p-t-75 p-b-85" >

		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
					<div class="m-l-25 m-r--38 m-lr-0-xl">
						{{-- <div class="wrap-table-shopping-cart"> --}}
                            <h3 class="p-lr-20">Billing Address</h3>
                            <div class="row">
                                <div class="col-12 col-md-6 p-t-40">
                                    <div class="form-group">
                                        <label class="p-lr-20 m-r-10">Name</label>
                                        <input required value=" {{Auth::user()->name}}" class="stext-104 bor13 cl2 plh4 size-117 w-100 p-lr-20 m-r-10 m-tb-5" type="text" name="firstname" placeholder="First Name">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 p-t-40">
                                    <div class="form-group">
                                        <label class="p-lr-20 m-r-10">Email</label>
                                        <input id="email-address" required value=" {{Auth::user()->email}}" class="stext-104 bor13 cl2 plh4 size-117 w-100 p-lr-20 m-r-10 m-tb-5" type="email" name="email" placeholder="Email">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 p-t-20">
                                    <div class="form-group">
                                        <label class="p-lr-20 m-r-10">Phone</label>
                                        <input id="phone" value="{{auth()->user()->phone}}" required class="stext-104 bor13 cl2 plh4 size-117 w-100 p-lr-20 m-r-10 m-tb-5" type="text" name="phone" placeholder="Phone">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 p-t-20">
                                    <div class="form-group">
                                        <label class="p-lr-20 m-r-10">Address</label>
                                        <input id="address" value="{{auth()->user()->address}}" required class="stext-104 bor13 cl2 plh4 size-117 w-100 p-lr-20 m-r-10 m-tb-5" type="text" name="address" placeholder="Address">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 p-t-20">
                                    <div class="form-group">
                                        <label class="p-lr-20 m-r-10">City</label>
                                        <input id="city" value="{{auth()->user()->city}}" required class="stext-104 bor13 cl2 plh4 size-117 w-100 p-lr-20 m-r-10 m-tb-5" type="text" name="city"  placeholder="City">
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 p-t-20">
                                    <div class="form-group">
                                        <label class="p-lr-20 m-r-10">State</label>
                                        {{-- <input  type="state"  placeholder="state"> --}}

										<select required class="stext-104 bor13 cl2 plh4 size-117 w-100 p-lr-20 m-r-10 m-tb-5" name="state" id="state">
											<option value="Abuja FCT">Abuja FCT</option>
											<option value="Abia">Abia</option>
											<option value="Adamawa">Adamawa</option>
											<option value="Akwa Ibom">Akwa Ibom</option>
											<option value="Anambra">Anambra</option>
											<option value="Bauchi">Bauchi</option>
											<option value="Bayelsa">Bayelsa</option>
											<option value="Benue">Benue</option>
											<option value="Borno">Borno</option>
											<option value="Cross River">Cross River</option>
											<option value="Delta">Delta</option>
											<option value="Ebonyi">Ebonyi</option>
											<option value="Edo">Edo</option>
											<option value="Ekiti">Ekiti</option>
											<option value="Enugu">Enugu</option>
											<option value="Gombe">Gombe</option>
											<option value="Imo">Imo</option>
											<option value="Jigawa">Jigawa</option>
											<option value="Kaduna">Kaduna</option>
											<option value="Kano">Kano</option>
											<option value="Katsina">Katsina</option>
											<option value="Kebbi">Kebbi</option>
											<option value="Kogi">Kogi</option>
											<option value="Kwara">Kwara</option>
											<option selected value="Lagos">Lagos</option>
											<option value="Nassarawa">Nassarawa</option>
											<option value="Niger">Niger</option>
											<option value="Ogun">Ogun</option>
											<option value="Ondo">Ondo</option>
											<option value="Osun">Osun</option>
											<option value="Oyo">Oyo</option>
											<option value="Plateau">Plateau</option>
											<option value="Rivers">Rivers</option>
											<option value="Sokoto">Sokoto</option>
											<option value="Taraba">Taraba</option>
											<option value="Yobe">Yobe</option>
											<option value="Zamfara">Zamfara</option>
											<option value="Outside Nigeria">Outside Nigeria</option>
										</select>

										<span>
											Free shipping within Lagos Nigeria Only. A sum of N3000 applies for shipping outside lagos
										</span>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="amount" id="amount"  value="{{ Cart::instance('default')->total() - (Cart::instance('default')->total()*($coupon/100)) }}" /> <!-- Replace the value with your transaction amount -->
                            <input type="hidden" name="payment_method" value="both" /> <!-- Can be card, account, both -->
                            <input type="hidden" name="description" value="Cart Payment" /> <!-- Replace the value with your transaction description -->
                            <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                            <input type="hidden" name="currency" value="NGN" />
                            <input type="hidden" name="logo" value="{{ asset('images/Mayor-04.png') }}" />
                            <input type="hidden" name="title" value="Mayorbranded" />

						{{-- </div> --}}

						{{-- <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
							<div class="flex-w flex-m m-r-20 m-tb-5">
								<input class="stext-104 cl2 plh4 size-117 bor13 p-lr-20 m-r-10 m-tb-5" type="text" name="coupon" placeholder="Coupon Code">

								<div class="flex-c-m stext-101 cl2 size-118 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-5">
									Apply coupon
								</div>
							</div>

							<div class="flex-c-m stext-101 cl2 size-119 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
								Update Cart
							</div>
						</div> --}}
					</div>
				</div>

				<div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
					<div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
						<h4 class="mtext-109 cl2 p-b-30">
							Items
                        </h4>

                        @foreach (Cart::instance('default')->content() as $item)
                            <div class="flex-w flex-t bor12 p-b-13 p-t-13">
                                <div class="size-208">
                                    <img class="img-fluid" src="{{ asset('storage/'.$item->model->image) }}" alt="IMG">
                                </div>

                                <div class="pl-2 size-209">
                                    <h5 class="mb-3"> <a href="#" style="color:#555" wire:click="$emit('showModal','{{ $item->model->id }}')" class="js-show-modal1">{{ $item->name }}</a></h5>
                                    <span style="color:#717fe0" class=" mtext-110 cl2">
                                        NGN {{ $item->model->price }}
                                    </span>
                                    <p style="color:#555; font-size:16px" class="mt-2 mtext-110 cl2">
                                        {{ $item->qty }} qty
                                    </p>
                                </div>
                            </div>
                        @endforeach

						<div class="flex-w flex-t bor12 p-t-15 p-b-30">
							<div class="size-208 w-full-ssm">
								<span class="stext-110 cl2">
									Shipping:
								</span>
							</div>

							{{-- <div class="size-209 p-r-18 p-r-0-sm w-full-ssm">
								<p class="stext-111 cl6 p-t-2">
									Free shipping within Lagos Nigeria Only. A sum of N500 applies for shipping outside lagos
								</p>

								<div class="p-t-15">
									<span class="stext-112 cl8">
										Calculate Shipping
									</span>

									<div class="rs1-select2 rs2-select2 bor8 bg0 m-b-12 m-t-9">
										<select required class="js-select2" name="time">
											<option>Select a State...</option>
											<option>within Lagos</option>
											<option>outside Lagos</option>
										</select>
										<div class="dropDownSelect2"></div>
									</div>

									<div class="bor8 bg0 m-b-12">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="state" placeholder="State /  country">
									</div>

									<div class="bor8 bg0 m-b-22">
										<input class="stext-111 cl8 plh3 size-111 p-lr-15" type="text" name="postcode" placeholder="Postcode / Zip">
									</div>

									<div class="flex-w">
										<div class="flex-c-m stext-101 cl2 size-115 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer">
											Update Totals
										</div>
									</div>

								</div>
							</div> --}}
						</div>

						<div  class="flex-w flex-t p-t-27 p-b-33">
							<div class="size-208">
								<span class="mtext-101 cl2">
									Sub-Total:
								</span>
							</div>

							<div class="size-209 p-t-1" style="text-align: right">
								<span class="mtext-110 cl2">
									NGN {{ Cart::instance('default')->total() - 0  }}
								</span>
							</div>
						</div>

						<div class="flex-w flex-t p-t-27 p-b-33">
							<div class="size-208">
								<span class="mtext-101 cl2">
									Total:
								</span>
							</div>

							<div class="size-209 p-t-1 " style="text-align: right">
								<span class="mtext-110 cl2">
									NGN {{ Cart::instance('default')->total() - (Cart::instance('default')->total()*($coupon/100)) }}
								</span>
							</div>
						</div>

						<button type="submit" class="mt-5 flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
							Pay
						</button>
					</div>
				</div>
			</div>
		</div>
	</form>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://js.paystack.co/v1/inline.js"></script>
    <script>
        const paymentForm = document.getElementById('paystcdForm');
        paymentForm.addEventListener("submit", payWithPaystack, false);

        function payWithPaystack(e) {
            e.preventDefault();
            let email =document.getElementById("email-address").value
            let phone =document.getElementById("phone").value
            let address =document.getElementById("address").value
            let city =document.getElementById("city").value
            let state =document.getElementById("state").value
            let amount = document.getElementById("amount").value * 100
            if(state != "Lagos"){
                amount = ( parseInt(document.getElementById("amount").value) + parseInt(3000))* 100
                alert(amount)
            }
            let OrderDetails ={
                phone: phone,
                address: address,
                city: city,
                state: state
            }
            let handler = PaystackPop.setup({
                key: 'pk_live_05ace59044b518c7e9ba7d0e63bcec4b426c46c6', // Replace with your public key
                email: email,
                amount: amount,

                ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                // label: "Optional string that replaces customer email"
                onClose: function(){
                    alert('Window closed.');
                },
                callback: (response) => {
                    let message = 'Payment complete! Reference: ' + response.reference;
                    alert(message);
                    window.location = "/paystack/verify/" + response.reference+"?details="+JSON.stringify(OrderDetails);
                }
            });
            handler.openIframe();
        }
    </script>
@endsection
