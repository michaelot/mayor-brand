@extends('layouts.wrapper')
@section('content')
    @livewire('header', ['activeLink' => "shop","version"=>"header-v4"])
    <!-- breadcrumb -->



	<!-- Product -->
	<div class="bg0 m-t-23 p-b-140">
		<div class="container">
			<div class="flex-w flex-sb-m p-b-52">
				<div class="flex-w flex-l-m filter-tope-group m-tb-10">
					<button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
						All Products
                    </button>

                    @foreach ($categories as $item)
                        <button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".{{ $item->name }}">
                            {{ $item->name }}
                        </button>
                    @endforeach

					
				</div>

				<div class="flex-w flex-c-m m-tb-10">
					<div class="flex-c-m stext-106 cl6 size-104 bor4 pointer hov-btn3 trans-04 m-r-8 m-tb-4 js-show-filter">
						<i class="icon-filter cl2 m-r-6 fs-15 trans-04 zmdi zmdi-filter-list"></i>
						<i class="icon-close-filter cl2 m-r-6 fs-15 trans-04 zmdi zmdi-close dis-none"></i>
						 Filter
					</div>

					<div class="flex-c-m stext-106 cl6 size-105 bor4 pointer hov-btn3 trans-04 m-tb-4 js-show-search">
						<i class="icon-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-search"></i>
						<i class="icon-close-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-close dis-none"></i>
						Search
					</div>
				</div>

				<!-- Search product -->
				<div class="dis-none panel-search w-full p-t-10 p-b-15">
					<div class="bor8 dis-flex p-l-15">
						<button class="size-113 flex-c-m fs-16 cl2 hov-cl1 trans-04">
							<i class="zmdi zmdi-search"></i>
						</button>

						<input class="mtext-107 cl2 size-114 plh2 p-r-15 quicksearch" type="text" name="search-product" placeholder="Search">
					</div>
				</div>

				<!-- Filter -->
				<div class="dis-none panel-filter w-full p-t-10">
					<div class="wrap-filter flex-w bg6 w-full p-lr-40 p-t-27 p-lr-15-sm">
						<div class="filter-col1 p-r-15 p-b-27">
							<div class="mtext-102 cl2 p-b-15">
								Sort By
							</div>

							<ul id="priceSort">
                                <li class="p-b-6">
									<a href="#" data-sort-by="original-order" class="filter-link-active priceSort filter-link stext-106 trans-04">
										Default
									</a>
								</li>

								<li class="p-b-6">
									<a href="#" data-sort-by="ascNew" class="priceSort filter-link stext-106 trans-04">
										New
									</a>
                                </li>

                                <li class="p-b-6">
									<a href="#" data-sort-by="descNew" class="priceSort filter-link stext-106 trans-04">
										Old
									</a>
								</li>

								<li class="p-b-6">
									<a href="#" data-sort-by="ascprice" data-direction="true" class="priceSort filter-link stext-106 trans-04">
										Price: Low to High
									</a>
								</li>

								<li class="p-b-6">
									<a href="#" data-sort-by="descprice" data-direction="false" class="priceSort filter-link stext-106 trans-04">
										Price: High to Low
									</a>
                                </li>
							</ul>
						</div>

						<div class="filter-col2 p-r-15 p-b-27">
							<div class="mtext-102 cl2 p-b-15">
								Size
							</div>

							<ul>
								<li class="p-b-6">
									<a href="#" class="filter-link size-filter stext-106 trans-04 filter-link-active" data-filter="*">
										All
									</a>
								</li>

								<li class="p-b-6">
									<a href="#" class="filter-link size-filter stext-106 trans-04" data-filter=".S">
										S
									</a>
								</li>

								<li class="p-b-6">
									<a href="#" class="filter-link size-filter stext-106 trans-04" data-filter=".M">
										M
									</a>
								</li>

								<li class="p-b-6">
									<a href="#" class="filter-link size-filter stext-106 trans-04" data-filter=".L">
										L
									</a>
								</li>

								<li class="p-b-6">
									<a href="#" class="filter-link size-filter stext-106 trans-04" data-filter=".XL">
										XL
									</a>
                                </li>

                                <li class="p-b-6">
									<a href="#" class="filter-link size-filter stext-106 trans-04" data-filter=".2XL">
										2XL
									</a>
								</li>

							</ul>
						</div>

						<div class="filter-col3 p-r-15 p-b-27">
							<div class="mtext-102 cl2 p-b-15">
								Color
							</div>

							<ul class="">
                                <li class="p-b-6">
									{{-- <span class="fs-15 lh-12 m-r-6" style="color: #222;">
										<i class="zmdi zmdi-circle"></i>
									</span> --}}

									<a href="#" class="filter-link m-l-20 color-filter stext-106 trans-04 text-center" data-filter="*">
										All
									</a>
								</li>
								<li class="p-b-6">
									<span class="fs-15 lh-12 m-r-6" style="color: #222;">
										<i class="zmdi zmdi-circle"></i>
									</span>

									<a href="#" class="filter-link  color-filter stext-106 trans-04" data-filter=".Black">
										Black
									</a>
								</li>

								<li class="p-b-6">
									<span class="fs-15 lh-12 m-r-6" style="color: #4272d7;">
										<i class="zmdi zmdi-circle"></i>
									</span>

									<a href="#" class="filter-link color-filter stext-106 trans-04 " data-filter=".Blue">
										Blue
									</a>
								</li>

								<li class="p-b-6">
									<span class="fs-15 lh-12 m-r-6" style="color: #b3b3b3;">
										<i class="zmdi zmdi-circle"></i>
									</span>

									<a href="#" class="filter-link color-filter stext-106 trans-04" data-filter=".Gray">
										Grey
									</a>
								</li>

								<li class="p-b-6">
									<span class="fs-15 lh-12 m-r-6" style="color: #00ad5f;">
										<i class="zmdi zmdi-circle"></i>
									</span>

									<a href="#" class="filter-link color-filter stext-106 trans-04" data-filter=".Green">
										Green
									</a>
								</li>

								<li class="p-b-6">
									<span class="fs-15 lh-12 m-r-6" style="color: #fa4251;">
										<i class="zmdi zmdi-circle"></i>
									</span>

									<a href="#" class="filter-link color-filter stext-106 trans-04" data-filter=".Red">
										Red
									</a>
								</li>

								<li class="p-b-6">
									<span class="fs-15 lh-12 m-r-6" style="color: #aaa;">
										<i class="zmdi zmdi-circle-o"></i>
									</span>

									<a href="#" class="filter-link color-filter stext-106 trans-04" data-filter=".White">
										White
									</a>
								</li>
							</ul>
						</div>

						{{-- <div class="filter-col4 p-b-27">
							<div class="mtext-102 cl2 p-b-15">
								Tags
							</div>

							<div class="flex-w p-t-4 m-r--5">
								<a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
									Fashion
								</a>

								<a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
									Lifestyle
								</a>

								<a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
									Denim
								</a>

								<a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
									Streetstyle
								</a>

								<a href="#" class="flex-c-m stext-107 cl6 size-301 bor7 p-lr-15 hov-tag1 trans-04 m-r-5 m-b-5">
									Crafts
								</a>
							</div>
						</div> --}}
					</div>
				</div>
			</div>

			<div class="row isotope-grid">
                @foreach ($products as $item)
                    <div data-price="{{ $item->price }}" data-id="{{ $item->id }}" class="col-sm-6 col-md-4 col-lg-3 p-b-35 @foreach ( json_decode($item->color) as $color) {{ $color}}  @endforeach @foreach ( json_decode($item->size) as $size) {{ $size}}  @endforeach {{ $item->pcategory->name}} isotope-item women">
                        <!-- Block2 -->
                        @livewire('product', ['product' => $item], key($item->id))
                    </div>
                @endforeach

			</div>

			<!-- Load more -->
			{{-- <div class="flex-c-m flex-w w-full p-t-45">
				<a href="#" class="flex-c-m stext-101 cl5 size-103 bg2 bor1 hov-btn1 p-lr-15 trans-04">
					Load More
				</a>
			</div> --}}
		</div>
	</div>
@endsection
