<div class="bg0 p-t-75 p-b-85">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-xl-7 m-lr-auto m-b-50">
                <div class="m-l-25 m-r--38 m-lr-0-xl">
                    <div class="wrap-table-shopping-cart">
                        <table class="table-shopping-cart">
                            <tr class="table_head">
                                <th class="column-1">Product</th>
                                <th class="column-2"></th>
                                <th class="column-3">Price</th>
                                <th class="column-4">Quantity</th>
                                <th class="column-5">Total</th>
                                <th class="column-6"></th>
                            </tr>
                            @if ($count>0)
                                {{-- {{ $test}} --}}
                                @foreach ($content as $row)
                                    {{-- {{ dd($row) }} --}}
                                    {{-- <p class="mb-4">{{json_encode($row)}}</p> --}}
                                    @livewire('cart-item', ['rowId' => $row->rowId], key($row->rowId))
                                @endforeach
                            @else
                                    <tr class="table_row">
                                        <td colspan='5' class="px-5 text-center">No item in your cart
                                            <a href=" {{route('home')}} " class="mt-4 text-white flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04 pointer">
                                            Continue Shoping
                                        </a>
                                    </td>
                                    </tr>
                            @endif

                        </table>
                    </div>
                    @if ($count>0)

                        <div class="flex-w flex-sb-m bor15 p-t-18 p-b-15 p-lr-40 p-lr-15-sm">
                            @if(Str::length($successMsg)>0)
                                <div class="py-2 w-full alert alert-success alert-dismissible fade show" role="alert">
                                    {{$successMsg}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if(Str::length($errorMsg)>0)
                                <div class="py-1 w-full alert alert-danger alert-dismissible fade show" role="alert">
                                    {{$errorMsg}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <div class="flex-w flex-m m-r-20 m-tb-5">
                                <input wire:model.defer="coupon" class="stext-104 cl2 plh4 size-117 bor13 p-lr-20 m-r-10 m-tb-5" type="text" name="coupon" placeholder="Coupon Code">

                                <button wire:click="applyCoupon" class="flex-c-m stext-101 cl2 size-118 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-5">
                                    Apply coupon
                                </button>
                            </div>
                            <a href="#" wire:click.prevent="clearCart" class="flex-c-m stext-101 cl2 size-118 bg8 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-5">
                                Clear Cart
                            </a>
                            {{-- <button  class="flex-c-m stext-101 cl2 size-119 bg6 bor13 hov-btn3 p-lr-15 trans-04 pointer m-tb-10">
                                Clear Cart
                            </button> --}}
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-sm-10 col-lg-7 col-xl-5 m-lr-auto m-b-50">
                <div class="bor10 p-lr-40 p-t-30 p-b-40 m-l-63 m-r-40 m-lr-0-xl p-lr-15-sm">
                    <h4 class="mtext-109 cl2 p-b-30">
                        Cart Totals
                    </h4>

                    <div class="flex-w flex-t bor12 p-b-13">
                        <div class="size-208">
                            <span class="stext-110 cl2">
                                Tax:
                            </span>
                        </div>

                        <div class="size-209">
                            <span class="mtext-110 cl2">
                                NGN     {{ Cart::tax() }}
                            </span>
                        </div>
                    </div>

                    <div class="flex-w flex-t bor12 p-t-27 p-b-13">
                        <div class="size-208">
                            <span class="stext-110 cl2">
                                Subtotal:
                            </span>
                        </div>

                        <div class="size-209">
                            <span class="mtext-110 cl2">
                                NGN {{ Cart::subTotal() }}
                            </span>
                        </div>
                    </div>

                    <div class="flex-w flex-t p-t-27 p-b-33">
                        <div class="size-208">
                            <span class="mtext-101 cl2">
                                Total:
                            </span>
                        </div>

                        <div class="size-209 p-t-1">
                            <span class="mtext-110 cl2">
                                NGN {{ Cart::subTotal() }}
                            </span>
                        </div>
                    </div>
                    {{-- {{Cart::instance('default')->count()}} --}}
                    <a @if(Cart::instance('default')->count()  != "0" )  href="{{route('checkout')}}" @endif class=" flex-c-m stext-101 cl0 size-116 bg3 bor14 hov-btn3 p-lr-15 trans-04">
                        Proceed to Checkout
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
