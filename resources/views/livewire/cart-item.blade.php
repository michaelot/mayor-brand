<tr class="table_row">
    <td class="column-1">
        <div class="how-itemcart1">
            <img src="{{ asset('storage/'.Cart::get($rowId)->model->image) }}" alt="IMG">
        </div>
    </td>
    <td class="column-2">
        <a href="#" style="color:#555" wire:click="$emit('showModal','{{ Cart::get($rowId)->model->id }}')" class="js-show-modal1">
            {{ Cart::get($rowId)->name }}
            <div>
                <span> size: {{Cart::get($rowId)->options->size}}</span>
            </div>
            <div>
                <span> color: {{Cart::get($rowId)->options->color}}</span>
            </div>
            <div>
                <span> Gender: {{Cart::get($rowId)->options->gender}}</span>
            </div>
        </a>
    </td>
    <td class="column-3">NGN {{Cart::get($rowId)->price}}</td>
    <td class="pr-0 column-4 ">
        <div class="wrap-num-product flex-w m-l-auto m-r-0">
            <div wire:click="decreaseQuantity()" class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
                <i class="fs-16 zmdi zmdi-minus"></i>
            </div>

            <input wire:model="quantity" class="mtext-104 cl3 txt-center num-product" type="number" name="num-product1" value="{{Cart::get($rowId)->qty}}">

            <div wire:click="increaseQuantity()" class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
                <i class="fs-16 zmdi zmdi-plus"></i>
            </div>
        </div>
    </td>
    <td style="width:110px !important" class="pr-2 column-5"> NGN {{Cart::get($rowId)->total}}</td>
    <td class="px-3 column-6" > <button wire:click='deleteProduct' type="button"> <i style="font-size:20px;color:#f74877" class="fa fa-trash-o"></i></button> </td>
</tr>
