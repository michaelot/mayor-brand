<div>
    @if ($showCart)
        <div class="header-cart flex-col-l p-l-65 p-r-25">
            <div class="header-cart-title flex-w flex-sb-m p-b-8">
                <span class="mtext-103 cl2">
                    Your Cart
                </span>

                <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                    <i class="zmdi zmdi-close"></i>
                </div>
            </div>

            <div class="header-cart-content flex-w js-pscroll">
                <div wire:loading>
                    <p class="text-center">Loading...</p>
                </div>
                <ul class="w-full header-cart-wrapitem">
                    @if ($cart > 0)
                        @foreach ($cartItems as $item)
                            <li class="header-cart-item flex-w flex-t m-b-12">
                                <div class="header-cart-item-img">
                                    <img src="{{ asset('storage/'.$item->model->image) }}" alt="IMG">
                                </div>

                                <div class="header-cart-item-txt p-t-8">
                                    <a href="#" wire:click="$emit('showModal','{{ $item->model->id }}')" class="header-cart-item-name m-b-18 hov-cl1 trans-04 js-show-modal1">
                                        {{ $item->model->name }}
                                    </a>

                                    <span class="header-cart-item-info">
                                        {{ $item->qty }} x NGN {{ $item->model->price }}
                                    </span>
                                </div>
                            </li>

                        @endforeach

                    @endif
                    @empty($cart)
                        <li wire:loading.remove class="header-cart-item-name m-b-18 hov-cl1 trans-04 js-show-modal1">
                            Your cart is empty
                        </li>
                    @endempty


                </ul>

                <div class="w-full">
                    <div class="w-full header-cart-total p-tb-40">
                        Total: NGN {{ $cartTotal }}
                    </div>

                    <div class="w-full header-cart-buttons flex-w">
                        <a href="{{ route('cart') }} " class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10">
                            View Cart
                        </a>

                        <a href="{{ route('checkout') }} " class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-b-10">
                            Check Out
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="header-cart flex-col-l p-l-65 p-r-25">
            <div class="header-cart-title flex-w flex-sb-m p-b-8">
                <span class="mtext-103 cl2">
                    Your Wishlist
                </span>

                <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
                    <i class="zmdi zmdi-close"></i>
                </div>
            </div>

            <div class="header-cart-content flex-w js-pscroll">
                <ul class="w-full header-cart-wrapitem">
                    @if ($wishlist > 0)
                        @foreach ($wishlistItems as $item)
                            <li class="header-cart-item flex-w flex-t m-b-12">
                                <div class="header-cart-item-img">
                                    <img src="{{ asset('storage/'.$item->model->image) }}" alt="IMG">
                                </div>

                                <div class="header-cart-item-txt p-t-8">
                                    <a href="#" wire:click="$emit('showModal','{{ $item->model->id }}')" class="header-cart-item-name m-b-18 hov-cl1 trans-04 js-show-modal1">
                                        {{ $item->model->name }}
                                    </a>

                                    <span class="header-cart-item-info">
                                        {{ $item->qty }} x NGN {{ $item->model->price }}
                                    </span>
                                </div>
                            </li>
                        @endforeach
                    @endif
                    @empty($wishlist)
                        <li wire:loading.remove class="header-cart-item-name m-b-18 hov-cl1 trans-04 js-show-modal1">
                            Your wishlist is empty
                        </li>
                    @endempty
                </ul>
            </div>
        </div>
    @endif
</div>
