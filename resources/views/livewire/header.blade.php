
<header class=" {{$version}} ">
    <!-- Header desktop -->

    <div class="container-menu-desktop">
        <!-- Topbar -->
        <script>
            window.addEventListener('contentChanged', event => {

                var headerDesktop = $('.container-menu-desktop');
                var wrapMenu = $('.wrap-menu-desktop');

                if($('.top-bar').length > 0) {
                    var posWrapHeader = $('.top-bar').height();
                }
                else {
                    var posWrapHeader = 0;
                }


                if($(window).scrollTop() > posWrapHeader) {
                    $(headerDesktop).addClass('fix-menu-desktop');
                    $(wrapMenu).css('top',0);
                }
                else {
                    $(headerDesktop).removeClass('fix-menu-desktop');
                    $(wrapMenu).css('top',posWrapHeader - $(this).scrollTop());
                }

                $(window).on('scroll',function(){
                    if($(this).scrollTop() > posWrapHeader) {
                        $(headerDesktop).addClass('fix-menu-desktop');
                        $(wrapMenu).css('top',0);
                    }
                    else {
                        $(headerDesktop).removeClass('fix-menu-desktop');
                        $(wrapMenu).css('top',posWrapHeader - $(this).scrollTop());
                    }
                });
            });
        </script>
        <div class="top-bar">
            <div class="container h-full content-topbar flex-sb-m">
                <div class="left-top-bar">
                    Free shipping within Lagos Nigeria Only
                </div>

                <div class="h-full right-top-bar flex-w">
                    <a href="#" class="flex-c-m trans-04 p-lr-25">
                        Help & FAQs
                    </a>
                    @guest
                        <a href=" {{ route('login') }} " class="flex-c-m trans-04 p-lr-25">
                            Login
                        </a>

                        <a href=" {{ route('register') }} " class="flex-c-m trans-04 p-lr-25">
                            Register
                        </a>

                    @endguest
                    @auth
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <a class="flex-c-m trans-04 p-lr-25" href="route('logout')"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('Logout') }}
                        </a>
                        </form>
                    @endauth

                    <a href="#" class="flex-c-m trans-04 p-lr-25">
                        NGN
                    </a>
                </div>
            </div>
        </div>

        <div class="wrap-menu-desktop">
            <nav class="container limiter-menu-desktop">

                <!-- Logo desktop -->
                <a href="/" class="logo" >
                    <img  src=" {{ asset('images/Mayor-04.png') }} " alt="IMG-LOGO">
                </a>

                <!-- Menu desktop -->
                <div class="menu-desktop m-l-50">
                    <ul class="main-menu">
                        <li class=" {{ $activeLink == 'home'? 'active-menu' :'' }}  ">
                            <a href=" {{ route('home') }} ">Home</a>
                            {{-- <ul class="sub-menu">
                                <li><a href="index.html">Homepage 1</a></li>
                                <li><a href="home-02.html">Homepage 2</a></li>
                                <li><a href="home-03.html">Homepage 3</a></li>
                            </ul> --}}
                        </li>

                        <li class=" {{ $activeLink == 'shop'? 'active-menu' :'' }}  ">
                            <a href="{{ route('shop') }}">Shop</a>
                        </li>

                        @auth
                            <li class="label1 {{ $activeLink == 'feature'? 'active-menu' :'' }}" data-label1=" {{ count(auth()->user()->orders->where('status','!=','Delivered')) }} ">
                                <a wire:click="$emit('showOrderModal')" class="js-show-modal1" >Track Orders</a>
                            </li>
                        @endauth



                        {{-- <li>
                            <a href="blog.html">Blog</a>
                        </li>

                        <li>
                            <a href="about.html">About</a>
                        </li>

                        <li>
                            <a href="contact.html">Contact</a>
                        </li> --}}
                    </ul>
                </div>

                <!-- Icon header -->
                <div class="wrap-icon-header flex-w flex-r-m">
                    {{-- <div {{ $activeLink == 'cart'? 'active-menu' :'' }} class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-modal-search">
                        <i class="zmdi zmdi-search"></i>
                    </div> --}}
                    {{-- href=" {{ route('cart') }} " --}}
                    <a wire:click="$emit('showCartModal','cart')" class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify="{{$cart}}">
                        <i class="zmdi zmdi-shopping-cart"></i>
                    </a>

                    <a wire:click="$emit('showCartModal','wishlist')" class="dis-block icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" data-notify="{{$wishlist}}">
                        <i class="zmdi zmdi-favorite-outline"></i>
                    </a>
                </div>
            </nav>
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap-header-mobile">
        <!-- Logo moblie -->
        <div class="logo-mobile">
            <a href="{{ route('home') }} ">
                <img src=" {{ asset('images/Mayor-04.png') }} " alt="IMG-LOGO">
            </a>
        </div>

        <!-- Icon header -->
        <div class="wrap-icon-header flex-w flex-r-m m-r-15">
            {{-- <div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 js-show-modal-search">
                <i class="zmdi zmdi-search"></i>
            </div> --}}

            <a wire:click="$emit('showCartModal','cart')" class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart" data-notify="{{$cart}}">
                <i class="zmdi zmdi-shopping-cart"></i>
            </a>

            <a wire:click="$emit('showCartModal','wishlist')" href="#" class="dis-block icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti" data-notify="{{$wishlist}}">
                <i class="zmdi zmdi-favorite-outline"></i>
            </a>
        </div>

        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </div>
    </div>


    <!-- Menu Mobile -->
    <div class="menu-mobile">
        <ul class="topbar-mobile">
            <li>
                <div class="left-top-bar">
                    Free shipping within Lagos Nigeria Only
                </div>
            </li>

            <li>
                <div class="h-full right-top-bar flex-w">
                    {{-- <a href="#" class="flex-c-m p-lr-10 trans-04">
                        Help & FAQs
                    </a> --}}
                    @guest
                        <a href=" {{ route('login') }} " class="flex-c-m p-lr-10 trans-04">
                            Login
                        </a>

                        <a href=" {{ route('register') }} " class="flex-c-m p-lr-10 trans-04">
                            Register
                        </a>
                    @endguest
                    @auth
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <a class="flex-c-m p-lr-10 trans-04" href="route('logout')"
                                    onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('Logout') }}
                        </a>
                        </form>
                    @endauth
                    <a href="#" class="flex-c-m p-lr-10 trans-04">
                        NGN
                    </a>
                </div>
            </li>
        </ul>

        <ul class="main-menu-m">
            <li>
                <a href=" {{ route('home') }} ">Home</a>

            </li>

            <li>
                <a href="{{ route('shop') }}">Shop</a>
            </li>
            @auth
                <li>
                    <a wire:click="$emit('showOrderModal')"  class="label1 rs1 js-show-modal1" data-label1="{{ count(auth()->user()->orders->where('status','!=','Delivered')) }}">Track Orders</a>
                </li>
            @endauth

            {{-- <li>
                <a href="blog.html">Blog</a>
            </li>

            <li>
                <a href="about.html">About</a>
            </li>

            <li>
                <a href="contact.html">Contact</a>
            </li> --}}
        </ul>
    </div>

    <!-- Modal Search -->
    <div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
        <div class="container-search-header">
            <button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
                <img src="images/icons/icon-close2.png" alt="CLOSE">
            </button>

            {{-- <form class="wrap-search-header flex-w p-l-15">
                <button class="flex-c-m trans-04">
                    <i class="zmdi zmdi-search"></i>
                </button>
                <input class="plh3" type="text" name="search" placeholder="Search...">
            </form> --}}
        </div>
    </div>


</header>
