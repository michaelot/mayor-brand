
<div class="block2" >
    <div class="block2-pic hov-img0">
        <img src=" {{ asset('storage/'.$product->image) }} " alt="IMG-PRODUCT">

        <a href="#" wire:click="$emit('showModal','{{ $product->id }}')" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
            Full View
        </a>
    </div>

    <div class="block2-txt flex-w flex-t p-t-14">
        <div class="block2-txt-child1 flex-col-l ">
            <a href="#" wire:click="$emit('showModal','{{ $product->id }}')" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6 js-show-modal1">
                {{ $product->name }}
            </a>

            <span class="stext-105 cl3">
                NGN <span class="price">{{ $product->price }}</span>
            </span>
        </div>

        <div class="block2-txt-child2 flex-r p-t-3">
            <a href="" wire:click.prevent='addToWishList' class=" dis-block pos-relative">
                @if ($liked)
                    <img class="icon-heart1 dis-block trans-04" src=" {{ asset('images/icons/icon-heart-02.png') }} " alt="ICON">
                @else
                    <img class="icon-heart1 dis-block trans-04" src=" {{ asset('images/icons/icon-heart-01.png') }} " alt="ICON">
                @endif

                {{-- <img class="icon-heart2 dis-block trans-04 ab-t-l" src=" {{ asset('images/icons/icon-heart-02.png') }} " alt="ICON"> --}}
            </a>
        </div>
    </div>
</div>


