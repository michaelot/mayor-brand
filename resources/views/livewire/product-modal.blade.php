<div>

    <script >
        window.addEventListener('contentChanged', event => {
            $(".js-select2").each(function(){

                $(this).select2({
                    minimumResultsForSearch: 20,
                    dropdownParent: $(this).next('.dropDownSelect2')
                });
            })


            $('.wrap-slick3').each(function(){
            $(this).find('.slick3').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                fade: true,
                infinite: true,
                autoplay: false,
                autoplaySpeed: 6000,

                arrows: true,
                appendArrows: $(this).find('.wrap-slick3-arrows'),
                prevArrow:'<button class="arrow-slick3 prev-slick3"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                nextArrow:'<button class="arrow-slick3 next-slick3"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',

                dots: true,
                appendDots: $(this).find('.wrap-slick3-dots'),
                dotsClass:'slick3-dots',
                customPaging: function(slick, index) {
                    var portrait = $(slick.$slides[index]).data('thumb');
                    return '<img src=" ' + portrait + ' "/><div class="slick3-dot-overlay"></div>';
                },
            });
        });

            $('.gallery-lb').each(function() { // the containers for all your galleries
                $(this).magnificPopup({
                    delegate: 'a', // the selector for gallery item
                    type: 'image',
                    gallery: {
                        enabled:true
                    },
                    mainClass: 'mfp-fade'
                });
            });

            $('.js-pscroll').each(function(){
                $(this).css('position','relative');
                $(this).css('overflow','hidden');
                var ps = new PerfectScrollbar(this, {
                    wheelSpeed: 1,
                    scrollingThreshold: 1000,
                    wheelPropagation: false,
                });

                $(window).on('resize', function(){
                    ps.update();
                })
            });
        });
    </script>
        <div class="overlay-modal1 js-hide-modal1"></div>

        <div class="container js-pscroll">

            <div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
                <button wire:click.prevent='closeModal' style="color:red;position: absolute; top: 5px; right: 20px; font-size: 30px;" class=" hov3 trans-04 js-hide-modal1">
                    <i class="zmdi zmdi-close"></i>
                </button>
                @if(!empty($product))
                    <div class="row">
                        <div class="col-md-6 col-lg-7 p-b-30">
                            <div class="p-l-25 p-r-30 p-lr-0-lg">
                                <div wire:ignore class="wrap-slick3 flex-sb flex-w">
                                    <div class="wrap-slick3-dots"></div>
                                    <div class="wrap-slick3-arrows flex-sb-m flex-w"></div>

                                    <div class="slick3 gallery-lb">
                                        <div class="item-slick3" data-thumb="{{ asset('storage/'.$product->image) }}">
                                            <div class="wrap-pic-w pos-relative">
                                                <img src="{{ asset('storage/'.$product->image) }}" alt="IMG-PRODUCT">

                                                <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="{{ asset('storage/'.$product->image) }}">
                                                    <i class="fa fa-expand"></i>
                                                </a>
                                            </div>
                                        </div>
                                        @foreach ( json_decode($product->additional_images) as $item)
                                            <div class="item-slick3" data-thumb="{{ asset('storage/'.$item) }}">
                                                <div class="wrap-pic-w pos-relative">
                                                    <img src="{{ asset('storage/'.$item) }}" alt="IMG-PRODUCT">

                                                    <a class="flex-c-m size-108 how-pos1 bor0 fs-16 cl10 bg0 hov-btn3 trans-04" href="{{ asset('storage/'.$item) }}">
                                                        <i class="fa fa-expand"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-5 p-b-30">
                            <div class="p-r-50 p-t-5 p-lr-0-lg">
                                @if(Str::length($successMsg)>0)
                                    <div class="py-2 alert alert-success alert-dismissible fade show" role="alert">
                                        {{$successMsg}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <h4 class="mtext-105 cl2 js-name-detail p-b-14">
                                    {{$product->name}}
                                </h4>

                                <span class="mtext-106 cl2">
                                    NGN{{$product->price}}
                                </span>

                                <p class="stext-102 cl3 p-t-23">
                                    {{$product->description}}
                                </p>

                                <!--  -->
                                @if (!$isInCart)
                                    <form method="post" wire:submit.prevent='addToCart' >
                                        <div class="p-t-33">

                                            <div class="flex-w flex-r-m p-b-10">
                                                <div class="size-203 flex-c-m respon6">
                                                    Size
                                                </div>

                                                <div class="size-204 respon6-next">
                                                    @error('size')
                                                    <div class="py-1 alert alert-warning alert-dismissible fade show" role="alert">
                                                        {{$message}}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    @enderror
                                                    <div class="rs1-select2 bor8 bg0">
                                                        <select  onChange="@this.setSizeVal(value)" class="js-select2" name="size">
                                                            <option  >Choose an option</option>
                                                            @foreach ( json_decode($product->size) as $item)
                                                                <option  @if($size == $item) selected @endif value="{{$item}}">{{$item}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="dropDownSelect2"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="flex-w flex-r-m p-b-10">
                                                <div class="size-203 flex-c-m respon6">
                                                    Color
                                                </div>

                                                <div class="size-204 respon6-next">
                                                    @error('color')
                                                    <div class="py-1 alert alert-warning alert-dismissible fade show" role="alert">
                                                        {{$message}}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    @enderror
                                                    <div class="rs1-select2 bor8 bg0">
                                                        <select onChange="@this.setColorVal(value)" class="js-select2" name="color">
                                                            <option >Choose an option</option>
                                                            @foreach ( json_decode($product->color) as $item)
                                                                <option @if($color == $item) selected @endif value="{{$item}}">{{$item}}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="dropDownSelect2"></div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="flex-w flex-r-m p-b-10">
                                                <div class="size-203 flex-c-m respon6">
                                                    Gender
                                                </div>

                                                <div class="size-204 respon6-next">
                                                    @error('gender')
                                                    <div class="py-1 alert alert-warning alert-dismissible fade show" role="alert">
                                                        {{$message}}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    @enderror
                                                    <div class="rs1-select2 bor8 bg0">
                                                        <select wire:ignore onChange="@this.setGenderVal(value)" class="js-select2" name="color">
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                        <div class="dropDownSelect2"></div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="flex-w flex-r-m p-b-10">
                                                <div class="size-204 flex-w flex-m respon6-next">
                                                    @error('quantity')
                                                    <div class="py-1 alert alert-warning alert-dismissible fade show" role="alert">
                                                        {{$message}}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    @enderror
                                                    <div class="wrap-num-product flex-w m-r-20 m-tb-10">
                                                        <button type="button" wire:click="reduceQuantity" class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
                                                            <i class="fs-16 zmdi zmdi-minus"></i>
                                                        </button>

                                                        <input wire:model.defer='quantity' class="mtext-104 cl3 txt-center num-product" type="number" name="quantity" >

                                                        <button type="button" wire:click="increaseQuantity" class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
                                                            <i class="fs-16 zmdi zmdi-plus"></i>
                                                        </button>
                                                    </div>


                                                    <button type="submit" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form> 
                                @endif
                                


                                <!--  -->
                                {{-- <div class="flex-w flex-m p-l-100 p-t-40 respon7">
                                    <div class="flex-m bor9 p-r-10 m-r-11">
                                        <a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 js-addwish-detail tooltip100" data-tooltip="Add to Wishlist">
                                            <i class="zmdi zmdi-favorite"></i>
                                        </a>
                                    </div>

                                    <a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Facebook">
                                        <i class="fa fa-facebook"></i>
                                    </a>

                                    <a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>

                                    <a href="#" class="fs-14 cl3 hov-cl1 trans-04 lh-10 p-lr-5 p-tb-2 m-r-8 tooltip100" data-tooltip="Google Plus">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-12" wire:loading>
                        <p class="text-center">Loading...</p>
                    </div>
                </div>
                @if ($showOrders)
                    <div class="row" >
                        <div class=" col-12">
                            <div class="py-2 mx-4">
                                <div class="bread-crumb flex-w p-l-25 p-r-15 p-t-30 p-lr-0-lg">
                                    <a href="index.html" class="stext-109 cl8 hov-cl1 trans-04">
                                        Home
                                        <i class="fa fa-angle-right m-l-9 m-r-10" aria-hidden="true"></i>
                                    </a>

                                    <span class="stext-109 cl4">
                                        Orders
                                    </span>
                                </div>
                                    <h3 class="text-center mtext-105 ">Your Orders</h3>
                            </div>
                        </div>
                        @foreach (auth()->user()->orders->where('status','!=','Delivered') as $order)
                            <div class=" col-lg-8 m-lr-auto">
                                <div class="px-3 py-4 mx-4 mt-3 order-div block1">
                                        <h4 class="d-flex justify-content-between"> <span>{{ $order->title }} {{ $order->created_at }}</span>  <span class="badge bg-info">{{ $order->status }}</span> </h4>
                                        <h5 style="color:#717fe0" class="mt-3 mb-5">NGN{{ $order->amount }}</h5>
                                        <div class="row">

                                            @php
                                                $shit=(Array) json_decode($order->cart);
                                            @endphp
                                            @foreach ( $shit as $item)
                                                    <div class="col-sm-6 col-md-4 p-b-35 isotope-item women">
                                                        <div class="block2">
                                                            <div class="block2-pic hov-img0">
                                                                <img src=" {{ asset('storage/'.$item->model->image) }} " alt="IMG-PRODUCT">

                                                                {{-- <a href="#" wire:click="$emit('showModal','{{ $item->model->id }}')" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                                    Full View
                                                                </a> --}}
                                                            </div>

                                                            <div class="block2-txt flex-w flex-t p-t-14">
                                                                <div class="block2-txt-child1 flex-col-l ">
                                                                    <a href="#" wire:click="$emit('showModal','{{ $item->model->id }}')" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6 js-show-modal1">
                                                                        {{ $item->model->name }}
                                                                    </a>

                                                                    <span class="stext-105 cl3">
                                                                        NGN {{ $item->model->price }}
                                                                    </span>
                                                                    <span class="stext-105 cl3">
                                                                        Qty {{ $item->qty }}, {{ $item->options->size }}, {{ $item->options->color }}
                                                                    </span>
                                                                </div>

                                                                {{--  --}}
                                                            </div>
                                                        </div>

                                                    </div>

                                            @endforeach
                                        </div>

                                </div>
                            </div>
                        @endforeach

                        <div class=" col-12">
                            <div class="py-2 mx-4">
                                <h3 class="mt-4 text-center mtext-105">Delivered Orders</h3>
                            </div>
                        </div>
                        @foreach (auth()->user()->orders->where('status','==','Delivered') as $order)
                            <div class=" col-lg-8 m-lr-auto">
                                <div class="px-3 py-4 mx-4 mt-3 order-div block1">
                                        <h4 class="d-flex justify-content-between"> <span>{{ $order->title }} {{ $order->created_at }}</span>  <span class="badge bg-info">{{ $order->status }}</span> </h4>
                                        <h5 style="color:#717fe0" class="mt-3 mb-5">NGN{{ $order->amount }}</h5>
                                        <div class="row">

                                            @php
                                            $shit=(Array) json_decode($order->cart);
                                            @endphp
                                            @foreach ( $shit as $item)
                                                    <div class="col-sm-6 col-md-4 p-b-35 isotope-item women">
                                                        <div class="block2">
                                                            <div class="block2-pic hov-img0">
                                                                <img src=" {{ asset('storage/'.$item->model->image) }} " alt="IMG-PRODUCT">

                                                                {{-- <a href="#" wire:click="$emit('showModal','{{ $item->model->id }}')" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1">
                                                                    Full View
                                                                </a> --}}
                                                            </div>

                                                            <div class="block2-txt flex-w flex-t p-t-14">
                                                                <div class="block2-txt-child1 flex-col-l ">
                                                                    <a href="#" wire:click="$emit('showModal','{{ $item->model->id }}')" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6 js-show-modal1">
                                                                        {{ $item->model->name }}
                                                                    </a>

                                                                    <span class="stext-105 cl3">
                                                                        NGN {{ $item->model->price }}
                                                                    </span>
                                                                    <span class="stext-105 cl3">
                                                                        Qty {{ $item->qty }}, {{ $item->options->size }}, {{ $item->options->color }}
                                                                    </span>
                                                                </div>

                                                                {{--  --}}
                                                            </div>
                                                        </div>

                                                    </div>

                                            @endforeach
                                        </div>

                                </div>
                            </div>
                        @endforeach

                    </div>
                @endif

            </div>
        </div>
</div>
